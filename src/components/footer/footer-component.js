(function(angular) {
    'use strict';
    var app = angular.module('eshoperApp');

    app.directive('footerWidget', function() {

        return {
            scope: {
                name: '@',
                data: '=data'
            },
            templateUrl: 'src/directives/eshopper-footer/eshopper-footerwidget-directive.html'
        }

    })

    var eshoperFooterCtrl = function($scope, $http) {
        $http.get('src/json/footerInfo.json').success(function(data) {
                $scope.serviceData = data;
            })
            .error(function() {
                alert(0);
            });
    }


    app.component('eshoperFooter', {
        templateUrl: 'src/components/footer/footer-component.html',
        controller: ['$scope', '$http', eshoperFooterCtrl]
    });
})(window.angular)