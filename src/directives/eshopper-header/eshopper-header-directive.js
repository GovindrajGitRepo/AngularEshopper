(function(angular) {
    'use strict';
    var app = angular.module('eshoperApp');
    app.directive('eshoperHeader', function() {

        return {

            templateUrl: 'src/directives/eshopper-header/eshopper-header-directive.html'

        }

    })
})(window.angular)