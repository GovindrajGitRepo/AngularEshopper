(function(angular) {
    'use strict';
    var app = angular.module('eshoperApp');
    app.directive('eshoperFooter', function() {

        return {
            scope: {
                name: '@'
            },
            templateUrl: 'src/directives/eshopper-footer/eshopper-footer-directive.html'
        }
    })
})(window.angular)